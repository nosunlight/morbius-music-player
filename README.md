# Morbius
The only music player for the undead creatures of the night
# GTK only ("for freedom purposes")

Might merge updates from blue-music [here](https://github.com/armytricks/blue-music/releases/latest)

# Future
- Add better icons.
- Add an about tab to the GUI.
- Will move to OrbTK when it is stable
- Clean up the codebase to make it more readable
- Future versions will be a complete rewrite so don't expect things to work properly.

# Not Going To Be Implemented
- MP3 support 1.) I'm lazy, 2.) I really don't know how to implement it. 3.) MP3 is for lozers.

# Requested Stuff
- Add a cool logo

# SUPER IMPORTANT STUFF
- I need help implementing MP3 so i can satisfy the normies
